//Load value from storage.
let browserStorage = (browser)?browser:chrome;
browserStorage.storage.local.get().then(
    (item) => document.getElementById('apiKey').value = (item.apiKey)?item.apiKey:"",
    (error) => console.log(`Error: ${error}`)
);
browserStorage.storage.local.get().then(
    (item) => document.getElementById('investmentAmount').value = (item.investmentAmount)?item.investmentAmount:"10000",
    (error) => console.log(`Error: ${error}`)
);

//Save value when user saves the new apikey value.
document.getElementById('saveBtn').onclick = function(){
    let browserStorage = (browser)?browser:chrome;
    browserStorage.storage.local.set({
        'apiKey': document.getElementById('apiKey').value,
        'investmentAmount': document.getElementById('investmentAmount').value
    }).then(
        (item) => document.getElementById('message').textContent = 'Settings saved successfuly!',
        (error) => console.log(`Error: ${error}`)
    );
};
