/**
 * Saves the dialog data into browser storage
 * @return {[type]} [description]
 */
var saveData = function() {
    let browserStorage = (browser) ? browser : chrome;
    browserStorage.storage.local.set({
        'strategy': document.getElementById('strategy').value,
        'minDate': document.getElementById('mindate').value,
        'maxDate': document.getElementById('maxdate').value,
        'minSharePrice': document.getElementById('minsp').value,
        'maxSharePrice': document.getElementById('maxsp').value
    }).then(
        (item) => {
            document.getElementById("results").textContent = 'Filters saved successfully!';
        },
        (error) => console.log(`Error: ${error}`)
    );
};

/**
 * Sends a message to front-end script to force refreshing current stocks list
 * according to filter
 *
 * @return {[type]} [description]
 */
var refreshStocksList = function() {
    let browserObj = (browser) ? browser : chrome;
    try {
        browserObj.tabs.query({
            active: true,
            currentWindow: true
        }, function(tabs) {
            var sending = chrome.tabs.sendMessage(tabs[0].id, {
                    action: "updateLeads"
                },
                (message) => console.log(`Message from the background script:  ${message.response}`));
        });
    } catch (e) {}
};

/**
 * Add event listeners to save on modif
 * @type {Date}
 */
document.getElementById('strategy').addEventListener('change', (event) => {
    switch (event.target.value) {
        case 'ma200crossoverma400':
            {
                //Set min/max date according to strategy...
                var minDate = new Date();
                minDate.setDate(minDate.getDate() - 2);
                document.getElementById("mindate").value = minDate.getYear() + '-' + minDate.getMonth() + '-' + minDate.getDay();
                var maxDate = new Date();
                document.getElementById("maxdate").value = maxDate.getYear() + '-' + maxDate.getMonth() + '-' + maxDate.getDay();
            }
            break;
    }
    saveData();
});
document.getElementById('mindate').addEventListener('change', (event) => {
    saveData();
});
document.getElementById('maxdate').addEventListener('change', (event) => {
    saveData();
});
document.getElementById('minsp').addEventListener('change', (event) => {
    saveData();
});
document.getElementById('maxsp').addEventListener('change', (event) => {
    saveData();
});
window.addEventListener("unload", function() {
    refreshStocksList();
    window.close();
});
document.getElementById('apply').addEventListener('click', (event) => {
    refreshStocksList();
    window.close();
});

/**
 * Populate fields on load.
 */
let getData = function() {
    let browserStorage = (browser) ? browser : chrome;
    browserStorage.storage.local.get().then(
        (item) => {
            document.getElementById('strategy').value = item.strategy;
            document.getElementById('mindate').value = item.minDate;
            document.getElementById('maxdate').value = item.maxDate;
            document.getElementById('minsp').value = item.minSharePrice;
            document.getElementById('maxsp').value = item.maxSharePrice;
        },
        (error) => console.log(`Error: ${error}`)
    );
};
getData();
