--------------------------------------------------------------------------------
Firefox extension for Stockfilter / QT integration
--------------------------------------------------------------------------------

Steps to sign and build extension:
    1.  Increase version number in manifest.json file.
    2.  Run this command as regular user in terminal, replace api-key and api-secret
        parameters by yours :

        web-ext sign --channel=unlisted --api-key=user:35434535:678... --api-secret=...

    3.  Your XPI file representing your extension will be available in
        <projectroot>/web-ext-artifacts/ folder
