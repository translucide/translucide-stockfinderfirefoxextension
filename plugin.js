var stockFilterForQuestradeExtension = {
    config: {
        'apiKey': null,
        'investmentAmount': 10000,
        'minDate': null,
        'maxDate': null,
        'minSharePrice': null,
        'maxSharePrice': null,
        'interval': null,
        'refreshInterval': 60,
        'leads': [],
        "leadsIndex": 0,
        'apiUrl': 'https://stockfilter.ca/en/stockfilter/api/v1'
    },
    init: function() {
        if (!window.jQuery) return null;
        var $this = stockFilterForQuestradeExtension;

        //Get API Key & filters
        let browserRef = (browser) ? browser : chrome;
        let configPromise = browserRef.storage.local.get().then($this.loadConfigSuccess,$this.loadConfigError);

        //Listen for changes in filters from BG Script.
        //Clear leads, ten reload them all
        let handleMessage = function(request, sender, sendResponse) {
            if (request.action == "updateLeads") {
                $this.config.leads = [];
                $this.config.leadsIndex = [];
                let configPromise = browserRef.storage.local.get().then($this.loadConfigSuccess,$this.loadConfigError);
            }
        };
        browserRef.runtime.onMessage.addListener(handleMessage);

        $this.setupUI();
    },
    getRangeContent: function(){
        try{
            var $this = stockFilterForQuestradeExtension;
            if ($this.config.leads.length === 0) {
                return "---";
            }
            else {
                const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                var date = new Date($this.config.leads[$this.config.leadsIndex]['timestamp'] * 1000);
                var dateStr = String(monthNames[date.getMonth()]).padStart(2,"0")+" "+String(date.getDate()).padStart(2, '0')+" "+String(date.getHours()).padStart(2, '0')+":"+String(date.getMinutes()).padStart(2, '0');
                return '<span class="position">'+($this.config.leadsIndex+1)+"/"+($this.config.leads.length)+'</span><span class="date">'+dateStr+'</span><span class="symbol">'+$this.config.leads[$this.config.leadsIndex]['symbol']+'@'+(Math.round($this.config.leads[$this.config.leadsIndex]['price'] * 100) / 100).toFixed(2)+'</span>';
            }
        }
        catch(e){
            return '';
        }
    },
    setupUI: function(){
        var $this = stockFilterForQuestradeExtension;
        if ($(".sf_prev_btn").length < 1) {
            //Setup UI elements
            if ($(".main-header .segmented-button__container").length < 1) {
                return;
            }
            else {
                console.log('Stockfiler: setup UI fired!');
                $(".sf_prev_btn").remove();
                $(".sf_range_btn").remove();
                $(".sf_next_btn").remove();
                $(".main-header .segmented-button__container").append('<button class="sf_prev_btn" alt="Previous SF lead">&lt;</button><span class="sf_range_btn">'+$this.getRangeContent()+'</span><button class="sf_next_btn" alt="Next SF lead">&gt;</button><div class="sf_all_stocks_dropdown closed"></div>');

                //Bind click handlers
                $(".sf_prev_btn").off("click").off("click",$this.prevBtnClick);
                $(".sf_prev_btn").off("click").on("click",$this.prevBtnClick);
                $(".sf_next_btn").off("click").off("click",$this.nextBtnClick);
                $(".sf_next_btn").off("click").on("click",$this.nextBtnClick);
                $(".sf_range_btn").off("click").on("click",$this.buyBtnClick);
            }
        }
    },
    loadConfigSuccess: function(item) {
        var $this = stockFilterForQuestradeExtension;
        if (!item.apiKey) {
            alert('Please set your API Key in the Stockfilter plugin preferences');
        }
        else {
            $.extend($this.config,item);
            if ($this.config.interval) {
                window.clearInterval($this.config.interval);
            }
            $this.refreshData();
            $this.config.interval = setInterval($this.refreshData, $this.config.refreshInterval*1000);
        }
    },
    loadConfigError: function(item) {
        console.error('Failed to load config!');
    },
    refreshData: function() {
        var $this = stockFilterForQuestradeExtension;

        //Connect to SF api
        $.ajaxSetup({
           headers:{
              'ACCESS-TOKEN': $this.config.apiKey
           }
        });
        let filters = {
            'filters': {
                'strategy': $this.config.strategy,
                'minDate': $this.config.minDate,
                'maxDate': $this.config.maxDate,
                'minSharePrice': $this.config.minSharePrice,
                'maxSharePrice': $this.config.maxSharePrice,
            }
        };
        console.log('Stockfiler: refreshData'+JSON.stringify(filters));
        $.get($this.config.apiUrl+'/leads',filters).done($this.refreshDataSuccess).fail($this.refreshDataError);
    },
    refreshDataSuccess: function(data){
        console.log('Stockfiler: refreshDataSuccess received '+JSON.stringify(data));
        var $this = stockFilterForQuestradeExtension;
        $this.setupUI();

        //Append new items to end of leads array to preserve current order so
        //that user can browse leads.
        if (data && data.response && data.response.leads) {
            //Append new leads, skip those already present.
            data.response.leads.forEach((a) => {
                var found = false;
                $this.config.leads.forEach((b) => {
                    if (b.symbol == a.symbol) {
                        found = true;
                    }
                });
                if (!found) {
                    $this.config.leads.push(a);
                }
            });

            $(".sf_range_btn").html($this.getRangeContent());

            let dropDownContent = "";
            $this.config.leads.foreach((a,i) => {
                dropDownContent += "<a class='sf_lead_badge"+(($this.config.leadsIndex == i)?" current":"")+"'>"+a.symbol+"</a>";
            });
            $(".sf_all_stocks_dropdown").html(dropDownContent);
            console.log("Stockfiler refreshDataSuccess leads after filtering : "+JSON.stringify($this.config.leads));
        }
    },
    refreshDataError: function(data){
        console.log('Stockfiler: refreshDataError = '+JSON.stringify(data));
    },
    prevBtnClick: function(e){
        var $this = stockFilterForQuestradeExtension;
        if ($this.config.leadsIndex < 1) {
            $this.config.leadsIndex = $this.config.leads.length - 1;
        }
        else {
            $this.config.leadsIndex--;
        }
        if ($this.config.leadsIndex > -1) {
            $this.changeCurrentStock();
        }
    },
    nextBtnClick: function(e){
        var $this = stockFilterForQuestradeExtension;
        if ($this.config.leadsIndex >= $this.config.leads.length-1) {
            $this.config.leadsIndex = 0;
        }
        else {
            $this.config.leadsIndex++;
        }
        if ($this.config.leadsIndex < $this.config.leads.length) {
            $this.changeCurrentStock();
        }
    },
    buyBtnClick: function(e){
        var $this = stockFilterForQuestradeExtension;
        var timeoutFnc = function() {
            try{
                //Set number of shares
                //$("[data-qt='boxQuantitySpinner'] i[data-qt='btnIncrement'].icon-plus").mousedown();
                //$("input[ng-model='vmQuantitySelector.model.value']").val("\t1234\n");
                $("input[ng-model='vmQuantitySelector.model.value']").val(parseInt($this.config.investmentAmount/$this.config.leads[$this.config.leadsIndex]['price']));
                //$("input[ng-model='vmQuantitySelector.model.value']").change().blur();

                //Set price
                $("input[name='limitPrice']").val($this.config.leads[$this.config.leadsIndex]['price']);
                //$("input[name='limitPrice']").change().blur();
            }
            catch(e) {
            }
        };
        setTimeout(timeoutFnc,350);
        $("button.buy-sell-btn.left").click();
    },
    changeCurrentStock: function(){
        var $this = stockFilterForQuestradeExtension;
        try{
            $(".symbol-lookup-field input").css("background-color",'#e2efd5');
            $(".symbol-lookup-field input").val($this.config.leads[$this.config.leadsIndex]['symbol']);
            window.history.pushState({},null, '/trading/quote/'+$this.config.leads[$this.config.leadsIndex]['symbol']+'/chart');

            //Update the label mentioning 1/450 (slider position)
            $(".sf_range_btn").html($this.getRangeContent());
        }
        catch(e) {
        }
    }
};

//Push execution just after everything running on load.
setTimeout(function() {
    stockFilterForQuestradeExtension.init();
});
